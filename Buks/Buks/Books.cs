﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Buks
{
    public class Books
    {
        private String name;
        private int year;
        private String author;
        private double price;

        public Books(String name, int year, String author, double price)
        {
            this.name = name;
            this.year = year;
            this.author = author;
            this.price = price;
        }

        public String getName()
        {
            return name;
        }

        public int getYear()
        {
            return year;
        }

        public String getAuthor()
        {
            return author;
        }

        public double getPrice()
        {
            return price;
        }
    }
}