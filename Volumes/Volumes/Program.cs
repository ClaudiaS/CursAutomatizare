﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Volumes
{
    class Program
    {
            public static void Main (String[] args)
        {

        }
            
             public static double GetVolume(int l )
            {
                // Cube Volume
                return l*l*l;
                Console.WriteLine("The volume of the the cube is " + GetVolume(l));
            }

            public static double GetVolume(double radius)
            {
                // Sphere Volume
                return 4.0 / 3 * Math.PI * Math.Pow(radius, 3);
                Console.WriteLine("The volume of the the sphere is " + GetVolume(radius));
            }

            public static double GetVolume(int length, int width, int height)
            {
                // Pyramid Volume
                    return length * width * height / 3;

                }

            public static double GetVolume(double radius, double height)
            {
                // Cylinder Volume
                return Math.PI * Math.Pow(radius, 2) * height;
                Console.WriteLine("The volume of the cylinder is " + GetVolume(radius, height));
            }

            public static double GetVolume(double length, double width, double height)
            {
                // Torus Volume
                return length * width * height;
                Console.WriteLine("The volume of the Torus is " + GetVolume(length, width, height));
            }

        }
        }
    


       