﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buks
{
    public class Program
    {
     
            public void Main()
            {
                Authors bookAuthors = new Authors("Sigmund Freud", "sfreud@library.com");
                Books newBook = new Books("Studies on Hysteria", 1937, "Sigmund Freud", 333.99);

                Console.WriteLine("Book " + newBook.getName() + " , price " + newBook.getPrice() + " $, by " + newBook.getAuthor() + ", published in year " + newBook.getYear() + ".");
              
            }
        }
    }
