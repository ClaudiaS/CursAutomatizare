﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareShapes
{
    class Program
    {
        
            public static void DrawFullShape(int width, int height)
            {

                for (int x = 1; x <= height; x++)

                {

                    for (int y = 1; y <= width; y++)
                    {
                        Console.Write("*");

                    }
                    Console.WriteLine();

                }
            }

            public static void DrawShapeOutline(int width, int height)
            {

                for (int x = 1; x <= height; x++)

                {

                    for (int y = 1; y <= width; y++)
                    {
                        if ((x == 1 || x == height) || (y == 1 || y == width))

                            Console.Write("*");

                        else

                            Console.Write(" ");
                    }
                    Console.WriteLine();
                }
            }

            public static void DrawShapeCorners(int width, int height)

            {

                for (int x = 1; x <= height; x++)

                {

                    for (int y = 1; y <= width; y++)
                    {
                        if ((x == 1 || x == height) && (y == 1 || y == width))

                            Console.Write("*");

                        else

                            Console.Write(" ");
                    }
                    Console.WriteLine();
                }
            }

            static void Main(string[] args)
            {

                DrawFullShape(int.Parse(args[0]), int.Parse(args[1]));
                Console.WriteLine();
                DrawShapeOutline(int.Parse(args[0]), int.Parse(args[1]));
                Console.WriteLine();
                DrawShapeCorners(int.Parse(args[0]), int.Parse(args[1]));
                Console.ReadKey();

            }
        }
    }


    

