﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AritmeticCalculator
{
    class Calculator
    {

        static void Main(String[] args)

        {

            if (args.Length == 3)
            {
                float a, b;

                a = float.Parse(args[0]);
                b = float.Parse(args[2]);
                string op = args[1];
                float result = 0;

                // implement calculator logic here
                switch (op)
                {

                    case "+":
                        result = a + b;
                        break;

                    case "-":
                        result = a - b;
                        break;

                    case "*":
                        result = a * b;
                        break;

                    case "/":
                        result = a / b;
                        break;

                    default:
                        Console.WriteLine("invalid input");
                        break;
                }
                Console.WriteLine("Result is: " + result);
                Console.ReadKey();

            }
            else
            {
                Console.WriteLine("3 args are needed!");
                Console.ReadKey();


            }
            Console.ReadKey();
        }
    }
}
