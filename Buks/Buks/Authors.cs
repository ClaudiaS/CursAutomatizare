﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buks
{
    public class Authors
    {
        private String name;
        private String email;

        public Authors(String name, String email)
        {
            this.name = name;
            this.email = email;
        }

        public String getName()
        {
            return name;
        }

        public String getEmail()
        {
            return email;
        }
    }
}