﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaSaptamana2
{
    class Program
    {
        static void Main(string[] args)
        {

            //number of days in a February month between 1900 - 2016

            int year;
            Console.Write("Enter a year between the 1900 and 2016: ");

            year = Convert.ToInt32(Console.ReadLine());

            if (year % 4 == 0 || year % 100 != 0 && year % 400 == 0)
            {
                Console.WriteLine(" Year " + year + " has 28 days in February");

            }
            else
            {
                Console.WriteLine("Year " + year + " has 29 days in February");

            }
            Console.ReadKey();

            
        }
    }
}