﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Program
    {
        
            static void Main(string[] args)
            {

                // suma primelor 100 de numbere positive
                int sum = 0;
                for (int i = 1; i <= 100; i++)
                {
                    sum += i;
                }
                Console.WriteLine("The sum of the first 100 numbers higher than 0 = " + sum);
                Console.ReadKey();


                //checking if a number is a palindrome
                int num, rem, s = 0, temp;
                //clrscr();
                Console.WriteLine("\n >>>> To Find a Number is Palindrome or not <<<< ");
                Console.Write("\n Enter a number: ");

                num = Convert.ToInt32(Console.ReadLine());
                temp = num;
                while (Convert.ToBoolean(num))
                {
                    rem = num % 10;  //for getting remainder by dividing with 10
                    num = num / 10; //for getting quotient by dividing with 10
                    s = s * 10 + rem; /*multiplying the sum with 10 and adding
                           remainder*/
                }
                Console.WriteLine("\n The Reversed Number is: {0} \n", s);
                if (temp == s) //checking whether the reversed number is equal to entered number
                {
                    Console.WriteLine("\n Number is Palindrome \n\n");
                }
                else
                {
                    Console.WriteLine("\n Number is not a palindrome \n\n");
                }
                Console.ReadLine();


                //all the prime numbers lower than a given number
                bool isPrime = true;
                Console.WriteLine("Insert a number to find all the prime numbers bellow: ");
                int n = Convert.ToInt32(Console.ReadLine()); ;
                for (int i = 2; i <= n; i++)
                {
                    for (int j = 2; j <= i; j++)
                    {

                        if (i != j && i % j == 0)
                        {
                            isPrime = false;
                            break;
                        }

                    }
                    if (isPrime)
                    {
                    
                        Console.Write("\t" + i);
                    }
                    isPrime = true;
                }
                Console.ReadKey();
            }
        }

    }

